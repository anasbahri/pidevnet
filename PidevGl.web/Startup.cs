﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PidevGl.web.Startup))]
namespace PidevGl.web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
