namespace PidevGl.domain.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("pidevgl.publication")]
    public partial class publication
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public publication()
        {
            jaime = new HashSet<jaime>();
        }

        [Key]
        public int idPub { get; set; }

        public DateTime? datePub { get; set; }

        [StringLength(255)]
        public string descPub { get; set; }

        [StringLength(255)]
        public string photoPub { get; set; }

        public int? user_id_user { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<jaime> jaime { get; set; }

        public virtual user user { get; set; }
    }
}
