namespace PidevGl.domain.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("pidevgl.jaime")]
    public partial class jaime
    {
        [Key]
        public int idJaime { get; set; }

        public int? publication_idPub { get; set; }

        public int? user_id_user { get; set; }

        public virtual publication publication { get; set; }

        public virtual user user { get; set; }
    }
}
