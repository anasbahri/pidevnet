namespace PidevGl.domain.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("pidevgl.chargehoraireprojet")]
    public partial class chargehoraireprojet
    {
        [Key]
        public int idCharge { get; set; }

        public DateTime? date { get; set; }

        public int nbrHeures { get; set; }

        public int nbrHeuresSupp { get; set; }

        public int seuilHeures { get; set; }

        public int? id_user { get; set; }

        public virtual user user { get; set; }
    }
}
