namespace PidevGl.domain.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("pidevgl.affectation")]
    public partial class affectation
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id_user { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int idmission { get; set; }

        public int periode { get; set; }

        public int semestre { get; set; }

        public int volumeHoraire { get; set; }

        public virtual mission mission { get; set; }

        public virtual user user { get; set; }
    }
}
