namespace PidevGl.domain.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("pidevgl.mission")]
    public partial class mission
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public mission()
        {
            affectation = new HashSet<affectation>();
            assignepp = new HashSet<assignepp>();
            facture = new HashSet<facture>();
        }

        [Key]
        public int idmission { get; set; }

        public DateTime? datedebut { get; set; }

        public DateTime? datefin { get; set; }

        [StringLength(255)]
        public string destination { get; set; }

        [StringLength(255)]
        public string etat { get; set; }

        [StringLength(255)]
        public string objectif { get; set; }

        [StringLength(255)]
        public string territoire { get; set; }

        [StringLength(255)]
        public string type { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<affectation> affectation { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<assignepp> assignepp { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<facture> facture { get; set; }
    }
}
