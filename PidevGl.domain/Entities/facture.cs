namespace PidevGl.domain.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("pidevgl.facture")]
    public partial class facture
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public facture()
        {
            assignepp = new HashSet<assignepp>();
        }

        [Key]
        public int idfacture { get; set; }

        public int Montant { get; set; }

        public int frais_extra { get; set; }

        public int frais_hebergement { get; set; }

        public int frais_restauration { get; set; }

        public int frais_transport { get; set; }

        [StringLength(255)]
        public string image { get; set; }

        public int somme { get; set; }

        [StringLength(255)]
        public string type { get; set; }

        public int? id_Emp { get; set; }

        public int? id_Mis { get; set; }

        public int? partenariat_idpartenaire { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<assignepp> assignepp { get; set; }

        public virtual user user { get; set; }

        public virtual mission mission { get; set; }

        public virtual partenariat partenariat { get; set; }
    }
}
