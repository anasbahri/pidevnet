namespace PidevGl.domain.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("pidevgl.skills")]
    public partial class skills
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public skills()
        {
            employecompetence = new HashSet<employecompetence>();
        }

        [Key]
        public int id_skills { get; set; }

        [StringLength(255)]
        public string PhotoSkills { get; set; }

        [StringLength(255)]
        public string libelle { get; set; }

        public double? nb_skill_int { get; set; }

        public int? id_Categorie { get; set; }

        public virtual categorieskills categorieskills { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<employecompetence> employecompetence { get; set; }
    }
}
