namespace PidevGl.domain.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("pidevgl.employecompetence")]
    public partial class employecompetence
    {
        [Key]
        public int id_EmployeCompetence { get; set; }

        public double? nbCompetence { get; set; }

        [StringLength(255)]
        public string niveau { get; set; }

        public int? id_skill { get; set; }

        public int? id_User { get; set; }

        public virtual user user { get; set; }

        public virtual skills skills { get; set; }
    }
}
