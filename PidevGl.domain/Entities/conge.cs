namespace PidevGl.domain.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("pidevgl.conge")]
    public partial class conge
    {
        [Key]
        public int idConge { get; set; }

        [StringLength(255)]
        public string cause { get; set; }

        public DateTime? dateDebut { get; set; }

        public DateTime? dateFin { get; set; }

        public int isValid { get; set; }

        public int? user_id_user { get; set; }

        public virtual user user { get; set; }
    }
}
