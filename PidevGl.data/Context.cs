using PidevGl.domain.Entities;

namespace PidevGl.data
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    [DbConfigurationType(typeof(MySql.Data.Entity.MySqlEFConfiguration))]
    public partial class Context : DbContext
    {
        public Context()
            : base("name=Context")
        {
        }

        public virtual DbSet<affectation> affectation { get; set; }
        public virtual DbSet<assignepp> assignepp { get; set; }
        public virtual DbSet<categorieskills> categorieskills { get; set; }
        public virtual DbSet<chargehoraireprojet> chargehoraireprojet { get; set; }
        public virtual DbSet<conge> conge { get; set; }
        public virtual DbSet<employecompetence> employecompetence { get; set; }
        public virtual DbSet<facture> facture { get; set; }
        public virtual DbSet<formation> formation { get; set; }
        public virtual DbSet<jaime> jaime { get; set; }
        public virtual DbSet<mission> mission { get; set; }
        public virtual DbSet<partenariat> partenariat { get; set; }
        public virtual DbSet<planing> planing { get; set; }
        public virtual DbSet<project> project { get; set; }
        public virtual DbSet<publication> publication { get; set; }
        public virtual DbSet<skills> skills { get; set; }
        public virtual DbSet<task> task { get; set; }
        public virtual DbSet<team> team { get; set; }
        public virtual DbSet<timesheet> timesheet { get; set; }
        public virtual DbSet<user> user { get; set; }
        public virtual DbSet<workday> workday { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            
        }
    }
}
